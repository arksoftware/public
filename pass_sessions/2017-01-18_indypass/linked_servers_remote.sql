--============================================================================--
--
--  A Lightning Talk on Linked Server Queries, Remote Scripts (2/2), v1.00
--
--  Jay Robinson, downshiftdata@gmail.com, @downshiftdata
--
--                              All examples use the AdventureWorks2014 database
--                   https://msftdbprodsamples.codeplex.com/releases/view/125550
--============================================================================--

-- AdventureWorks2014 exists on both the local and helsinki.
USE AdventureWorks2014;
GO
SET STATISTICS IO ON;
SET STATISTICS TIME ON;
GO

--  Turn on 'optimize ad hoc workloads' for both.
EXECUTE sp_configure 'Optimize for Ad hoc Workloads';
GO
EXECUTE sp_configure 'Show Advanced Options', 1;
RECONFIGURE;
EXECUTE sp_configure 'Optimize for Ad hoc Workloads', 1;
RECONFIGURE;
GO

-- Flush all dirty pages from the buffer to disk.
CHECKPOINT;
-- Flush the data and procedure caches.
DBCC DROPCLEANBUFFERS;
DBCC FREEPROCCACHE;
GO

-- Examine the query plan cache.
SELECT
		cp.objtype,
		qp.query_plan,
		cp.cacheobjtype,
		cp.size_in_bytes,
		qs.[execution_count],
		qp.query_plan.value(N'(//@StatementText)[1]', N'NVARCHAR(MAX)')
	FROM sys.dm_exec_cached_plans AS cp
		CROSS APPLY sys.dm_exec_query_plan(cp.plan_handle) AS qp
		LEFT JOIN sys.dm_exec_query_stats AS qs
			ON cp.[plan_handle] = qs.[plan_handle]
	OPTION (RECOMPILE);
GO
