--============================================================================--
--
--  A Lightning Talk on Linked Server Queries, Demo Scripts (1/2), v1.00
--
--  Jay Robinson, downshiftdata@gmail.com, @downshiftdata
--
--                              All examples use the AdventureWorks2014 database
--                   https://msftdbprodsamples.codeplex.com/releases/view/125550
--============================================================================--




-- 1. Add a linked server and pass-through authentication.

IF NOT EXISTS (SELECT 1 FROM sys.servers WHERE [name] = N'my_remote')
	EXEC sp_addlinkedserver @server = N'my_remote', @srvproduct = N'', @provider = N'SQLNCLI', @datasrc = N'192.168.56.102';
GO

IF NOT EXISTS (SELECT 1 FROM sys.servers AS s INNER JOIN sys.linked_logins AS ll ON s.[name] = N'my_remote' AND s.[server_id] = ll.[server_id])
	EXEC sp_addlinkedsrvlogin @rmtsrvname = N'my_remote';
GO




-- 2. Perform some initial setup for the demos.

-- Use AdventureWorks2014 on both the local and my_remote.
USE AdventureWorks2014;
GO
-- Turn on statistics for both.
SET STATISTICS IO ON;
SET STATISTICS TIME ON;
GO
-- Turn on 'optimize ad hoc workloads' for both.
EXECUTE sp_configure 'Optimize for Ad hoc Workloads';
GO
EXECUTE sp_configure 'Show Advanced Options', 1;
RECONFIGURE;
EXECUTE sp_configure 'Optimize for Ad hoc Workloads', 1;
RECONFIGURE;
GO
-- Finally, turn on the Actual Execution Plan (Ctrl+M).




-- 3. A simple example.
SELECT * FROM my_remote.AdventureWorks2014.Production.Product;




-- 4. A more complicated example.

-- All Local
SELECT soh.SalesOrderID, soh.OrderDate, p.ProductID, p.Name, sod.OrderQty, sod.UnitPrice
	FROM Sales.SalesOrderHeader AS soh
		INNER JOIN Sales.SalesOrderDetail AS sod
			ON soh.SalesOrderID = sod.SalesOrderID
		INNER JOIN Production.Product AS p
			ON sod.ProductID = p.ProductID
	WHERE soh.CustomerID = 26745
	ORDER BY soh.SalesOrderID, p.ProductID;

-- Linked Server
SELECT soh.SalesOrderID, soh.OrderDate, p.ProductID, p.Name, sod.OrderQty, sod.UnitPrice
	FROM Sales.SalesOrderHeader AS soh
		INNER JOIN Sales.SalesOrderDetail AS sod
			ON soh.SalesOrderID = sod.SalesOrderID
		INNER JOIN my_remote.AdventureWorks2014.Production.Product AS p
			ON sod.ProductID = p.ProductID
	WHERE soh.CustomerID = 26745
	ORDER BY soh.SalesOrderID, p.ProductID;

-- Remote Join
SELECT soh.SalesOrderID, soh.OrderDate, p.ProductID, p.Name, sod.OrderQty, sod.UnitPrice
	FROM Sales.SalesOrderHeader AS soh
		INNER JOIN Sales.SalesOrderDetail AS sod
			ON soh.SalesOrderID = sod.SalesOrderID
		INNER REMOTE JOIN my_remote.AdventureWorks2014.Production.Product AS p
			ON sod.ProductID = p.ProductID
	WHERE soh.CustomerID = 26745
	ORDER BY soh.SalesOrderID, p.ProductID;

-- OPENQUERY
SELECT soh.SalesOrderID, soh.OrderDate, p.ProductID, p.Name, sod.OrderQty, sod.UnitPrice
	FROM Sales.SalesOrderHeader AS soh
		INNER JOIN Sales.SalesOrderDetail AS sod
			ON soh.SalesOrderID = sod.SalesOrderID
		INNER JOIN OPENQUERY(my_remote, 'SELECT ProductID, Name FROM AdventureWorks2014.Production.Product;') AS p
			ON sod.ProductID = p.ProductID
	WHERE soh.CustomerID = 26745
	ORDER BY soh.SalesOrderID, p.ProductID;




-- 5. An even more complicated example.

-- Linked Server
SELECT soh.SalesOrderID, soh.OrderDate, p.ProductID, p.Name, sod.OrderQty, sod.UnitPrice
	FROM my_remote.AdventureWorks2014.Sales.SalesOrderHeader AS soh
		INNER JOIN my_remote.AdventureWorks2014.Sales.SalesOrderDetail AS sod
			ON soh.SalesOrderID = sod.SalesOrderID
		INNER JOIN Production.Product AS p
			ON sod.ProductID = p.ProductID
	WHERE soh.CustomerID = 26745
	ORDER BY soh.SalesOrderID, p.ProductID;

-- Remote Join
SELECT soh.SalesOrderID, soh.OrderDate, p.ProductID, p.Name, sod.OrderQty, sod.UnitPrice
	FROM my_remote.AdventureWorks2014.Sales.SalesOrderHeader AS soh
		INNER REMOTE JOIN my_remote.AdventureWorks2014.Sales.SalesOrderDetail AS sod
			ON soh.SalesOrderID = sod.SalesOrderID
		INNER JOIN Production.Product AS p
			ON sod.ProductID = p.ProductID
	WHERE soh.CustomerID = 26745
	ORDER BY soh.SalesOrderID, p.ProductID;

-- OPENQUERY
SELECT so.SalesOrderID, so.OrderDate, p.ProductID, p.Name, so.OrderQty, so.UnitPrice
	FROM OPENQUERY(my_remote, 'SELECT soh.SalesOrderID, soh.OrderDate, sod.ProductID, sod.OrderQty, sod.UnitPrice
	FROM AdventureWorks2014.Sales.SalesOrderHeader AS soh
		INNER JOIN AdventureWorks2014.Sales.SalesOrderDetail AS sod
			ON soh.SalesOrderID = sod.SalesOrderID
			AND soh.CustomerID = 26745;') AS so
		INNER JOIN Production.Product AS p
			ON so.ProductID = p.ProductID
	ORDER BY so.SalesOrderID, p.ProductID;




-- 6. Problem A: The query in OPENQUERY must be a fixed string, which prevents parameterization of the local query.

-- This does not work.
SELECT so.SalesOrderID, so.OrderDate, p.ProductID, p.Name, so.OrderQty, so.UnitPrice
	FROM OPENQUERY(my_remote, 'SELECT soh.SalesOrderID, soh.OrderDate, sod.ProductID, sod.OrderQty, sod.UnitPrice
	FROM AdventureWorks2014.Sales.SalesOrderHeader AS soh
		INNER JOIN AdventureWorks2014.Sales.SalesOrderDetail AS sod
			ON soh.SalesOrderID = sod.SalesOrderID
			AND soh.CustomerID = ' + CAST(26745 AS NVARCHAR(20)) + ');') AS so
		INNER JOIN Production.Product AS p
			ON so.ProductID = p.ProductID
	ORDER BY so.SalesOrderID, p.ProductID;

-- Neither does this:
DECLARE @stmt VARCHAR(4000);
SELECT @stmt = 'SELECT soh.SalesOrderID, soh.OrderDate, sod.ProductID, sod.OrderQty, sod.UnitPrice
	FROM AdventureWorks2014.Sales.SalesOrderHeader AS soh
		INNER JOIN AdventureWorks2014.Sales.SalesOrderDetail AS sod
			ON soh.SalesOrderID = sod.SalesOrderID
			AND soh.CustomerID = ' + CAST(26745 AS NVARCHAR(20)) + ');';
SELECT so.SalesOrderID, so.OrderDate, p.ProductID, p.Name, so.OrderQty, so.UnitPrice
	FROM OPENQUERY(my_remote, @stmt) AS so
		INNER JOIN Production.Product AS p
			ON so.ProductID = p.ProductID
	ORDER BY so.SalesOrderID, p.ProductID;
GO

-- But this does:
DECLARE @stmt NVARCHAR(4000);
SELECT @stmt = N'SELECT so.SalesOrderID, so.OrderDate, p.ProductID, p.Name, so.OrderQty, so.UnitPrice
	FROM OPENQUERY(my_remote, ''SELECT soh.SalesOrderID, soh.OrderDate, sod.ProductID, sod.OrderQty, sod.UnitPrice
	FROM AdventureWorks2014.Sales.SalesOrderHeader AS soh
		INNER JOIN AdventureWorks2014.Sales.SalesOrderDetail AS sod
			ON soh.SalesOrderID = sod.SalesOrderID
			AND soh.CustomerID = ' + CAST(26745 AS NVARCHAR(20)) + ''') AS so
		INNER JOIN Production.Product AS p
			ON so.ProductID = p.ProductID
	ORDER BY so.SalesOrderID, p.ProductID;';
EXEC (@stmt);
GO




-- 7. Problem B: SQL Server will not parameterize OPENQUERY queries. *

-- This results in four compiled query plans.
SELECT * FROM OPENQUERY(my_remote, 'SELECT CustomerID, COUNT(*) FROM AdventureWorks2014.Sales.SalesOrderHeader WHERE CustomerID = 26745 GROUP BY CustomerID;');
SELECT * FROM OPENQUERY(my_remote, 'SELECT CustomerID, COUNT(*) FROM AdventureWorks2014.Sales.SalesOrderHeader WHERE CustomerID = 26746 GROUP BY CustomerID;');
SELECT * FROM OPENQUERY(my_remote, 'SELECT CustomerID, COUNT(*) FROM AdventureWorks2014.Sales.SalesOrderHeader WHERE CustomerID = 26747 GROUP BY CustomerID;');
SELECT * FROM OPENQUERY(my_remote, 'SELECT CustomerID, COUNT(*) FROM AdventureWorks2014.Sales.SalesOrderHeader WHERE CustomerID = 26748 GROUP BY CustomerID;');
GO

-- And so does this.
SELECT * FROM OPENQUERY(my_remote, 'EXEC (''SELECT CustomerID, COUNT(*) FROM AdventureWorks2014.Sales.SalesOrderHeader WHERE CustomerID = 26745 GROUP BY CustomerID;'');');
SELECT * FROM OPENQUERY(my_remote, 'EXEC (''SELECT CustomerID, COUNT(*) FROM AdventureWorks2014.Sales.SalesOrderHeader WHERE CustomerID = 26746 GROUP BY CustomerID;'');');
SELECT * FROM OPENQUERY(my_remote, 'EXEC (''SELECT CustomerID, COUNT(*) FROM AdventureWorks2014.Sales.SalesOrderHeader WHERE CustomerID = 26747 GROUP BY CustomerID;'');');
SELECT * FROM OPENQUERY(my_remote, 'EXEC (''SELECT CustomerID, COUNT(*) FROM AdventureWorks2014.Sales.SalesOrderHeader WHERE CustomerID = 26748 GROUP BY CustomerID;'');');
GO

-- But this doesn't.
SELECT * FROM OPENQUERY(my_remote, 'EXEC sp_executesql @stmt = N''SELECT CustomerID, COUNT(*) FROM AdventureWorks2014.Sales.SalesOrderHeader WHERE CustomerID = @cid GROUP BY CustomerID;'', @params = N''@cid INT'', @cid = 26745;');
SELECT * FROM OPENQUERY(my_remote, 'EXEC sp_executesql @stmt = N''SELECT CustomerID, COUNT(*) FROM AdventureWorks2014.Sales.SalesOrderHeader WHERE CustomerID = @cid GROUP BY CustomerID;'', @params = N''@cid INT'', @cid = 26746;');
SELECT * FROM OPENQUERY(my_remote, 'EXEC sp_executesql @stmt = N''SELECT CustomerID, COUNT(*) FROM AdventureWorks2014.Sales.SalesOrderHeader WHERE CustomerID = @cid GROUP BY CustomerID;'', @params = N''@cid INT'', @cid = 26747;');
SELECT * FROM OPENQUERY(my_remote, 'EXEC sp_executesql @stmt = N''SELECT CustomerID, COUNT(*) FROM AdventureWorks2014.Sales.SalesOrderHeader WHERE CustomerID = @cid GROUP BY CustomerID;'', @params = N''@cid INT'', @cid = 26748;');
GO

-- * ALTER DATABASE [master AND AdventureWorks2014] SET PARAMETERIZATION FORCED;




-- 8. So let's put them together!

-- The remote query is parameterized in the query plan cache.
-- The local query relies on 'optimize for ad hoc workloads', but OPTION (RECOMPILE) would also work.
DECLARE @stmt NVARCHAR(4000);
DECLARE @cid_local INT;
SELECT @cid_local = 26745;
SELECT @stmt = N'/* Fully Dynamic OPENQUERY - Local */
SELECT so.SalesOrderID, so.OrderDate, p.ProductID, p.Name, so.OrderQty, so.UnitPrice
	FROM OPENQUERY(my_remote, ''/* Fully Dynamic OPENQUERY - Remote */
EXEC sp_executesql
	@stmt = N''''/* Fully Dynamic OPENQUERY - Remote Ad Hoc */
SELECT soh.SalesOrderID, soh.OrderDate, sod.ProductID, sod.OrderQty, sod.UnitPrice
	FROM AdventureWorks2014.Sales.SalesOrderHeader AS soh
		INNER JOIN AdventureWorks2014.Sales.SalesOrderDetail AS sod
			ON soh.SalesOrderID = sod.SalesOrderID
			AND soh.CustomerID = @cid_remote;'''',
	@params = N''''@cid_remote INT'''', @cid_remote = ' + CAST(@cid_local AS NVARCHAR(20)) + ';'') AS so
		INNER JOIN Production.Product AS p
			ON so.ProductID = p.ProductID
	ORDER BY so.SalesOrderID, p.ProductID;';
EXEC (@stmt);
GO




-- 9. Problem C: OPENQUERY supports ASCII strings only.

-- Solution? There isn't one! Go back to REMOTE join hints!




-- https://twitter.com/downshiftdata/status/710889938661871617
